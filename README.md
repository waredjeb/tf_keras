# Tensorflow & Keras tutorial

Simple tutorial on tensorflow and keras. The purpose of this notebook is to show how to build a machine learning algorithm using tensorflow and keras. In particular, multi-layer-perceptron are implemented. Regularization techniques are also treated.
The tutorial-notebook is <b>tf_keras_tuto.ipynb<b>
